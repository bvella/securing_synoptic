﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using securing_synoptic.Utility;
using System;
using System.Drawing;
using System.IO;
using System.Net.Mime;
using WebApplication1.Utility;

namespace securing_synoptic.Controllers
{
    public class SteganographyController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        public SteganographyController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        [HttpGet()]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost("/steganography/encode")]
        public IActionResult encode(IFormFile file, string plaintext,string password)
        {
            string ext = file.FileName;
            if (password == null || file == null || file.Length == 0 || file.Length > 10000000 || !".jpg".Equals(ext.Substring(ext.Length - 4)))
            {
                return BadRequest();
            }
            Bitmap img;

            using (var memoryStream = new MemoryStream())
            {
                file.CopyTo(memoryStream);
                Image tempImg = Image.FromStream(memoryStream);
                img = (Bitmap)tempImg;
            }
            string enctxt = Encryption.SymmetricEncrypt(password + plaintext);
            img = SteganographyHelper.embedText(enctxt, img);
            var stream = new MemoryStream();
            img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
            string format = "image/png";
            stream.Seek(0, SeekOrigin.Begin);
            FileStreamResult res = base.File(stream, format); ;
            res.FileDownloadName = "steganodecode.jpg";
            return res;

        }
        [HttpPost("/steganography/decode")]
        public string decode(IFormFile file, string password)
        {
            string plaintext;
            string ext = file.FileName;
            if (password == null || file == null || file.Length == 0  || file.Length > 10000000 || !".jpg".Equals(ext.Substring(ext.Length - 4)))
            {
                return "BadRequest()";
            }
            Bitmap img;
            using (var memoryStream = new MemoryStream())
            {
                file.CopyTo(memoryStream);
                Image tempImg = Image.FromStream(memoryStream);
                img = (Bitmap)tempImg;
            }
            string enctxt = SteganographyHelper.extractText(img);
            plaintext = Encryption.SymmetricDecrypt(enctxt);
            if (password.Equals(plaintext.Substring(0, password.Length))) {
                System.Diagnostics.Debug.WriteLine(plaintext.Substring(password.Length));
                return plaintext.Substring(password.Length);
            }
            else
            {
                return "Invalid password";
            }
            
            

        }
    }
}
