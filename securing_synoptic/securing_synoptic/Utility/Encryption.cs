﻿using System;

using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace WebApplication1.Utility
{
    public class Encryption
    {

        static string password = "donotguess125!";
        static byte[] salt = new byte[]
        {
            20, 1, 34,56,78,34,11,111,234,43,180,139,127,34,52,45,255,253,1
        };

        public static byte[] SymmetricEncrypt(byte[] plaintext)
        {
            Rijndael myAlg = Rijndael.Create();
            var keys = GenerateKeys();
            MemoryStream msIn = new MemoryStream(plaintext);
            msIn.Position = 0;
            MemoryStream msOut = new MemoryStream();
            CryptoStream cs = new CryptoStream(msOut, myAlg.CreateEncryptor(keys.SecretKey, keys.Iv),CryptoStreamMode.Write);
            msIn.CopyTo(cs);
            cs.FlushFinalBlock();
            cs.Close();
            return msOut.ToArray();

        }

        public static SymmetricKeys GenerateKeys()
        {
            Rijndael myAlg = Rijndael.Create();
            Rfc2898DeriveBytes myGenerator = new Rfc2898DeriveBytes(password, salt);
            SymmetricKeys keys = new SymmetricKeys()
            { //1 byte =  8 bits
                SecretKey = myGenerator.GetBytes(myAlg.KeySize / 8),
                Iv = myGenerator.GetBytes(myAlg.BlockSize / 8)
            };

            return keys;
        }
        public static byte[] SymmetricDecrypt(byte[] cipherAsBytes)
        {
            Rijndael myAlg = Rijndael.Create();
            var keys = GenerateKeys();
            MemoryStream msIn = new MemoryStream(cipherAsBytes);
            msIn.Position = 0;
            MemoryStream msOut = new MemoryStream();
            CryptoStream cs = new CryptoStream(msOut,myAlg.CreateDecryptor(keys.SecretKey, keys.Iv),CryptoStreamMode.Write);
            msIn.CopyTo(cs);
            cs.FlushFinalBlock();
            cs.Close();

            return msOut.ToArray();
        }

        public static string SymmetricEncrypt(string clearData)
        {
            byte[] clearDataAsBytes = Encoding.UTF32.GetBytes(clearData);
            byte[] cipherAsBytes = SymmetricEncrypt(clearDataAsBytes);
            string cipher = Convert.ToBase64String(cipherAsBytes);
            return cipher;
        }

        public static string SymmetricDecrypt(string cipher)
        {
            byte[] cipherDataAsBytes = Convert.FromBase64String(cipher);
            byte[] clearDataAsBytes = SymmetricDecrypt(cipherDataAsBytes);
            string originalText = Encoding.UTF32.GetString(clearDataAsBytes);

            return originalText;
        }
    }

    public class SymmetricKeys
    {
        public byte[] SecretKey { get; set; }
        public byte[] Iv { get; set; }
    }
}
